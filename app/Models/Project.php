<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function associes()
    {
        //return $this->belongsTo('App\User');
    }
}
