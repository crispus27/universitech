<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domaine extends Model
{
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event');
    }

    public function formations()
    {
        return $this->hasMany('App\Models\Formation');
    }

    public function magazines()
    {
        return $this->hasMany('App\Models\Magazine');
    }

}
