<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    public function domaine()
    {
        return $this->belongsTo('App\Models\Domaine');
    }
}
