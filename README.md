1-Cloner le projet


# Installation des dependances avec composer 
2-[ composer install]  ##Pour installer les dependances

3-[composer dumpautoload -o ]


# Environment
4-[ cp .env.example .env]  *Copy the environment template*

# Générez la clé pour votre fichier d'environnement

5-[php artisan key:generate]


# Creer la base de données

6-[ Creer la base de données universitech]


# Editer le fichier .env  
7-Modifier les elements suivant celon votre environnement

* [DB_DATABASE=universitech ]
* [DB_USERNAME=root ]
* [DB_PASSWORD= ]


# Terminez en effaçant la configuration et générez le cache.

* 8-[ php artisan config:clear]
* 9-[ php artisan config:cache]

# Installer les dependences javascript

10-[npm install]

# Migration et seeding
*créer les migrations et les seeds(generates fake data for you)*

11-php artisan migrate:fresh --seed

# Se connecter
* *le mot de passe pour tous les utulisateurs est :*  **password**
* *l'email  :*  **Verifier dans la base de donnée**
