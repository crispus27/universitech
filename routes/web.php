<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('domaine', 'DomaineController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* ------------------------------ Espace Formation  Debut--------------------------------- */
Route::get('/formation/index', 'FormationController@index')->name('formation.index');
Route::get('/formation/create', 'FormationController@create')->name('formation.create');
Route::get('/formation/store', 'FormationController@store')->name('formation.store');
Route::get('/formation/show/{id}', 'FormationController@index')->name('formation.index');
Route::get('/formation/edit', 'FormationController@index')->name('formation.index');
Route::get('/formation/upadate/{id}', 'FormationController@index')->name('formation.index');
Route::get('/formation/destroy/{id}', 'FormationController@index')->name('formation.index');


/* ------------------------------ Espace Formation  Fin--------------------------------- */


/* ------------------------------ Espace Event  Debut--------------------------------- */

/* ------------------------------ Espace Events  Fin--------------------------------- */


/* ------------------------------ Espace Publication  Debut--------------------------------- */

/* ------------------------------ Espace Publication  Fin--------------------------------- */


/* ------------------------------ Espace Magazine  Debut--------------------------------- */

/* ------------------------------ Espace Magazine  Fin--------------------------------- */


/* ------------------------------ Espace Inscription  Debut--------------------------------- */

/* ------------------------------ Espace Inscription  Fin--------------------------------- */


/* ------------------------------ Espace Project  Debut--------------------------------- */

/* ------------------------------ Espace Project  Fin--------------------------------- */





/* ------------------------------ Espace administration  Debut--------------------------------- */

/* ------------------------------ Espace administration  FIn--------------------------------- */
