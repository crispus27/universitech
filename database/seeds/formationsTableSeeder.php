<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class formationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $formation = new \App\Models\Formation();
            $formation->title = $faker->name;
            $formation->enabled = $faker->boolean;
            $formation->url_video = "https://www.youtube.com/watch?v=mYzXj3Xs1Gs";
            $formation->description = $faker->text($maxNbChars = 2000);
            $formation->make_at = $faker->dateTime;
            $formation->domaine_id = $faker->numberBetween($min = 1, $max = 6);
            $formation->slug = str_slug('formation'.str_random(6).''.time(),'-');
            $formation->created_at  = \Carbon\Carbon::now();
            $formation->save();
        }
    }
}
