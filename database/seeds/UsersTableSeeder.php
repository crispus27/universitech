<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $favoris = "" ;
        $faker = Faker::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $user = new \App\User;
            $user->name = $faker->name;
            $user->email = $faker->unique()->email;
            $user->password = bcrypt('password');
            $user->telephone = $faker->phoneNumber;
            $user->image = $faker->imageUrl(400, 300);
            $user->image_cover = $faker->imageUrl(1000, 800 );
            $user->role_id = $faker->numberBetween($min = 1, $max = 2);
            $user->sexe = $faker->title($gender = 'male'|'female');
            $user->university = $faker->title;
            $user->enabled = $faker->boolean;
            $user->favoris = "{'2','3','4','5'}";
            $user->domaine_id = $faker->numberBetween($min = 2, $max = 6);
            $user->save();
        }
    }
}
