<?php

use Illuminate\Database\Seeder;

class rolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create([
            'name' => 'admin',
            'display_name' => 'Administrateur'
        ]);

        \App\Models\Role::create([
            'name' => 'user',
            'display_name' => 'Utilisateur normal'
        ]);
    }
}
