<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MagazinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $magazine = new \App\Models\Magazine();
            $magazine->title = $faker->name;
            $magazine->enabled = $faker->boolean;
            $magazine->url_video = "https://www.youtube.com/watch?v=RZdR5p_BsEo";
            $magazine->description = $faker->text($maxNbChars = 2000);
            $magazine->make_at = $faker->dateTime;
            $magazine->domaine_id = $faker->numberBetween($min = 3, $max = 7);
            $magazine->slug = str_slug('magazine'.str_random(6).''.time(),'-');
            $magazine->created_at  = \Carbon\Carbon::now();
            $magazine->save();
        }
    }
}
