<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
            $this->call(rolesTableSeeder::class);
            $this->call(domaineTableSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(ProjectsTableSeeder::class);
            $this->call(formationsTableSeeder::class);
            $this->call(MagazinesTableSeeder::class);
            $this->call(EventsTableSeeder::class);
           // $this->call(UsersTableSeeder::class);

    }
}
