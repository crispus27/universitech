<?php

use Illuminate\Database\Seeder;
use App\Models\Domaine;

class domaineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Domaine::create([
            'name' => 'Information'
        ]);
        Domaine::create([
            'name' => 'Agronomie'
        ]);
        Domaine::create([
            'name' => 'Electricité'
        ]);
        Domaine::create([
            'name' => 'Energie'
        ]);
        Domaine::create([
            'name' => 'Batiment'
        ]);
        Domaine::create([
            'name' => 'Logistique'
        ]);
        Domaine::create([
            'name' => 'Hotellerie'
        ]);
        Domaine::create([
            'name' => 'Industrie'
        ]);
    }
}
