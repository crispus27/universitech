<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $event = new \App\Models\Event();
            $event->title = $faker->name;
            $event->enabled = $faker->boolean;
            $event->url_video = "https://www.youtube.com/watch?v=PZLImCftLeM";
            $event->description = $faker->text($maxNbChars = 2000);
            $event->make_at = $faker->dateTime;
            $event->start_at = $faker->dateTime;
            $event->end_at = $faker->dateTime;
            $event->domaine_id = $faker->numberBetween($min = 1, $max = 5);
            $event->slug = str_slug('event'.str_random(6).''.time(),'-');
            $event->created_at  = \Carbon\Carbon::now();
            $event->save();
        }
    }
}
