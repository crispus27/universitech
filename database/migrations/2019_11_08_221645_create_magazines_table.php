<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagazinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magazines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->boolean('enabled');
            $table->string('slug');
            $table->string('url_video');
            $table->longtext('description');
            $table->timestamp('make_at')->nullable();

            $table->unsignedBigInteger('domaine_id');
            $table->foreign("domaine_id")
                ->references("id")
                ->on('domaines');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magazines');
    }
}
