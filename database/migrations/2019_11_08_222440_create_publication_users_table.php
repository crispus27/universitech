<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publication_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->boolean('isliked');

            $table->unsignedBigInteger('user_id');
            $table->foreign("user_id")
                ->references("id")
                ->on('users')
                ->onDelete('cascade');

            $table->unsignedBigInteger('publication_id');
            $table->foreign("publication_id")
                ->references("id")
                ->on('publications')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publication_users');
    }
}
